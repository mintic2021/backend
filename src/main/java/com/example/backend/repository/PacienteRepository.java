package com.example.backend.repository;

import java.util.List;

import com.example.backend.models.PacienteModel;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PacienteRepository extends CrudRepository<PacienteModel, Long> {

    @Query("select p from PacienteModel p where p.cedula = ?1")
    List <PacienteModel> findByCedula(Long cedula);    
}