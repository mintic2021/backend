package com.example.backend.models;

import javax.persistence.*;

@Entity
@Table(name = "history")
public class HistoryModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private long id;
    private String name;
    private long identification;
    private String birthDay;
    private String phone;
    private String sex;
    private int age;
    private String eps;
    private String medical;
    private String dateMedical;
    private String timerMedical;
    private int weight;
    private int height;
    private String imc;
    
    private String statusAlergia;
    private String alergia;

    private String statusAntecedente;
    private String antecendente;

    private String statusFuma;
    private String timesFuma;

    private String statusExercise;
    private String timesExercise;

    private String statusCirugia;
    private String cirugias;

    public long getIdentification() {
        return this.identification;
    }

    public void setIdentification(long identification) {
        this.identification = identification;
    }

    public String getBirthDay() {
        return this.birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public String getSex() {
        return this.sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getAge() {
        return this.age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEps() {
        return this.eps;
    }

    public void setEps(String eps) {
        this.eps = eps;
    }

    public String getMedical() {
        return this.medical;
    }

    public void setMedical(String medical) {
        this.medical = medical;
    }

    public String getDateMedical() {
        return this.dateMedical;
    }

    public void setDateMedical(String dateMedical) {
        this.dateMedical = dateMedical;
    }

    public String getTimerMedical() {
        return this.timerMedical;
    }

    public void setTimerMedical(String timerMedical) {
        this.timerMedical = timerMedical;
    }

    public int getWeight() {
        return this.weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getImc() {
        return this.imc;
    }

    public void setImc(String imc) {
        this.imc = imc;
    }

    public String getStatusAlergia() {
        return this.statusAlergia;
    }

    public void setStatusAlergia(String statusAlergia) {
        this.statusAlergia = statusAlergia;
    }

    public String getAlergia() {
        return this.alergia;
    }

    public void setAlergia(String alergia) {
        this.alergia = alergia;
    }

    public String getStatusAntecedente() {
        return this.statusAntecedente;
    }

    public void setStatusAntecedente(String statusAntecedente) {
        this.statusAntecedente = statusAntecedente;
    }

    public String getAntecendente() {
        return this.antecendente;
    }

    public void setAntecendente(String antecendente) {
        this.antecendente = antecendente;
    }

    public String getStatusFuma() {
        return this.statusFuma;
    }

    public void setStatusFuma(String statusFuma) {
        this.statusFuma = statusFuma;
    }

    public String getTimesFuma() {
        return this.timesFuma;
    }

    public void setTimesFuma(String timesFuma) {
        this.timesFuma = timesFuma;
    }

    public String getStatusExercise() {
        return this.statusExercise;
    }

    public void setStatusExercise(String statusExercise) {
        this.statusExercise = statusExercise;
    }

    public String getTimesExercise() {
        return this.timesExercise;
    }

    public void setTimesExercise(String timesExercise) {
        this.timesExercise = timesExercise;
    }

    public String getStatusCirugia() {
        return this.statusCirugia;
    }

    public void setStatusCirugia(String statusCirugia) {
        this.statusCirugia = statusCirugia;
    }

    public String getCirugias() {
        return this.cirugias;
    }

    public void setCirugias(String cirugias) {
        this.cirugias = cirugias;
    }

    public String getStatusHospital() {
        return this.statusHospital;
    }

    public void setStatusHospital(String statusHospital) {
        this.statusHospital = statusHospital;
    }

    public String getMotivo() {
        return this.motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getObservations() {
        return this.observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public String getExamenes() {
        return this.examenes;
    }

    public void setExamenes(String examenes) {
        this.examenes = examenes;
    }

    public String getFormula() {
        return this.formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    private String statusHospital;

    private String motivo;
    private String observations;
    private String examenes;
    private String formula;

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}
