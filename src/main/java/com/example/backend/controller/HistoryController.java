package com.example.backend.controller;

import com.example.backend.models.HistoryModel;
import com.example.backend.services.HistoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMethod;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE })
@RequestMapping("/history")
public class HistoryController {

    @Autowired
    HistoryService historyService;

    // almacenar ó actualizar un registro
    @PostMapping
    public HistoryModel saveStudent(@RequestBody HistoryModel student) {
        return historyService.saveHistory(student);
    }

    // Obtener todos los registro
    @GetMapping
    public ArrayList<HistoryModel> getHistories() {
        return historyService.getHistories();
    }

    // Obtener registro por cédula
    @GetMapping(path = "/{id}")
    public List<HistoryModel> getHistoriesByCedula(@PathVariable("id") Long id) {
        return this.historyService.getHistoriesByCedula(id);
    }

    // Eliminar registro por cédula
    @DeleteMapping(path = "/{id}")
    public boolean deleteById(@PathVariable("id") Long id) {
        return this.historyService.deleteById(id);
    }

}