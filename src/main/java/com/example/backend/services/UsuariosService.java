package com.example.backend.services;

import com.example.backend.models.UsuariosModel;
import com.example.backend.repository.UsuariosRepository;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UsuariosService {

    @Autowired
    UsuariosRepository usuariosRepository;

    // obtener todos los registros
    public ArrayList<UsuariosModel> getUsuarios() {
        return (ArrayList<UsuariosModel>) usuariosRepository.findAll();
    }

    // guardar /actualizar Usuarios
    public UsuariosModel saveUsuarios(UsuariosModel usuarios) {
        return usuariosRepository.save(usuarios);
    }

    // obtener registro por correo
    public List<UsuariosModel> findByEmail(String email) {
        return usuariosRepository.findByEmail(email);
    }

    // eliminar registro por id
    public boolean deleteById(Long id) {
        usuariosRepository.deleteById(id);
        return true;
    }

}
