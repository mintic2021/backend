package com.example.backend.repository;

import java.util.List;

import com.example.backend.models.UsuariosModel;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuariosRepository extends CrudRepository<UsuariosModel, Long> {
    
    @Query("select u from UsuariosModel u where u.email = ?1")
    List <UsuariosModel> findByEmail(String email);

}