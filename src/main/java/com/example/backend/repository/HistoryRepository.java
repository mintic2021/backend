package com.example.backend.repository;

import java.util.List;

import com.example.backend.models.HistoryModel;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface HistoryRepository extends CrudRepository<HistoryModel, Long> {

    @Query("select h from HistoryModel h where h.identification = ?1")
    List <HistoryModel> findByCedula(Long id);
}