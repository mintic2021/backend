package com.example.backend.controller;

import com.example.backend.models.PacienteModel;
import com.example.backend.services.PacienteService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.RequestMethod;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE })
@RequestMapping("/paciente")
public class PacienteController {

    @Autowired
    PacienteService pacienteService;

    @PostMapping
    public PacienteModel savePaciente(@RequestBody PacienteModel paciente) {
        return pacienteService.savePaciente(paciente);
    }

    @GetMapping
    public ArrayList<PacienteModel> getPaciente() {
        return pacienteService.getPaciente();
    }

    @GetMapping(path = "/{cedula}")
    public List<PacienteModel> findByCedula(@PathVariable("cedula") Long cedula) {
        return this.pacienteService.findByCedula(cedula);
    }

    @DeleteMapping(path = "/{id}")
    public boolean deleteById(@PathVariable("id") Long id) {
        return this.pacienteService.deleteById(id);
    }

}