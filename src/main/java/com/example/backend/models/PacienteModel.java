package com.example.backend.models;

import javax.persistence.*;

@Entity
@Table(name = "paciente")
public class PacienteModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private long id;
    private String nombre;
    private long cedula;
    private String nacimiento;
    private String celular;
    private String sexo;
    private int edad;
    private String eps;
    private String otraeps;
    private String medicinaPrepagada;
    private String otramed;
    private String estadocivil;
    private int hijos;
    private int cuantoshijos;
    private String direccion;
    private String ciudad;
    private String otraciudad;
    private String nombreContacto;
    private String parentesco;
    private String celularContacto;




    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public long getCedula() {
        return this.cedula;
    }

    public void setCedula(long cedula) {
        this.cedula = cedula;
    }

    public String getNacimiento() {
        return this.nacimiento;
    }

    public void setNacimiento(String nacimiento) {
        this.nacimiento = nacimiento;
    }

    public String getCelular() {
        return this.celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getSexo() {
        return this.sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public int getEdad() {
        return this.edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getEps() {
        return this.eps;
    }

    public void setEps(String eps) {
        this.eps = eps;
    }

    public String getOtraeps() {
        return this.otraeps;
    }

    public void setOtraeps(String otraeps) {
        this.otraeps = otraeps;
    }

    public String getMedicinaPrepagada() {
        return this.medicinaPrepagada;
    }

    public void setMedicinaPrepagada(String medicinaPrepagada) {
        this.medicinaPrepagada = medicinaPrepagada;
    }

    public String getOtramed() {
        return this.otramed;
    }

    public void setOtramed(String otramed) {
        this.otramed = otramed;
    }

    public String getEstadocivil() {
        return this.estadocivil;
    }

    public void setEstadocivil(String estadocivil) {
        this.estadocivil = estadocivil;
    }

    public int getHijos() {
        return this.hijos;
    }

    public void setHijos(int hijos) {
        this.hijos = hijos;
    }

    public int getCuantoshijos() {
        return this.cuantoshijos;
    }

    public void setCuantoshijos(int cuantoshijos) {
        this.cuantoshijos = cuantoshijos;
    }

    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudad() {
        return this.ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getOtraciudad() {
        return this.otraciudad;
    }

    public void setOtraciudad(String otraciudad) {
        this.otraciudad = otraciudad;
    }

    public String getNombreContacto() {
        return this.nombreContacto;
    }

    public void setNombreContacto(String nombreContacto) {
        this.nombreContacto = nombreContacto;
    }

    public String getParentesco() {
        return this.parentesco;
    }

    public void setParentesco(String parentesco) {
        this.parentesco = parentesco;
    }

    public String getCelularContacto() {
        return this.celularContacto;
    }

    public void setCelularContacto(String celularContacto) {
        this.celularContacto = celularContacto;
    }
    

}