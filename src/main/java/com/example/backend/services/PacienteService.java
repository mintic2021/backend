package com.example.backend.services;

import com.example.backend.models.PacienteModel;
import com.example.backend.repository.PacienteRepository;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@Service
public class PacienteService {
    

    @Autowired
    PacienteRepository pacienteRepository;

    //obtener todos los registros
    public ArrayList<PacienteModel> getPaciente(){
        return (ArrayList<PacienteModel>) pacienteRepository.findAll();
    }

    //obtener registro por cédula
    public List<PacienteModel> findByCedula(Long cedula) {
        return pacienteRepository.findByCedula(cedula);
    }

    //guardar / actualizar  Paciente
    public PacienteModel savePaciente(PacienteModel paciente) {
        return pacienteRepository.save(paciente);
    }
    
    //eliminar un paciente
    public boolean deleteById(Long id) {
        pacienteRepository.deleteById(id);
        return true;
    }


}
