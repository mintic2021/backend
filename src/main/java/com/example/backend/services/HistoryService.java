package com.example.backend.services;

import com.example.backend.models.HistoryModel;
import com.example.backend.repository.HistoryRepository;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class HistoryService {

    @Autowired
    HistoryRepository historyRepository;

    // obtener todos los registros
    public ArrayList<HistoryModel> getHistories() {
        return (ArrayList<HistoryModel>) historyRepository.findAll();
    }

    // guardar historia
    public HistoryModel saveHistory(HistoryModel history) {
        return historyRepository.save(history);
    }

    // obtener registro por cédula
    public List<HistoryModel> getHistoriesByCedula(Long id) {
        return historyRepository.findByCedula(id);
    }

    public boolean deleteById(Long id) {
        historyRepository.deleteById(id);
        return true;
    }

}
