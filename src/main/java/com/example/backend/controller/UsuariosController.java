package com.example.backend.controller;

import com.example.backend.models.UsuariosModel;
import com.example.backend.services.UsuariosService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.RequestMethod;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE })
@RequestMapping("/usuarios")
public class UsuariosController {

    @Autowired
    UsuariosService usuariosService;

    @PostMapping
    public UsuariosModel saveUsuarios(@RequestBody UsuariosModel usuarios) {
        return usuariosService.saveUsuarios(usuarios);
    }

    @GetMapping
    public ArrayList<UsuariosModel> getUsuarios() {
        return usuariosService.getUsuarios();
    }

    // Obtener registro por cédula
    @GetMapping(path = "/{email}")
    public List<UsuariosModel> findByEmail(@PathVariable("email") String email) {
        return this.usuariosService.findByEmail(email);
    }

    // Eliminar registro por cédula
    @DeleteMapping(path = "/{id}")
    public boolean deleteById(@PathVariable("id") Long id) {
        return this.usuariosService.deleteById(id);
    }

}